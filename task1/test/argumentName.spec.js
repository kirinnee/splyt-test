const chai = require("chai"),
	{getArgumentName} = require("../src/argumentName");

chai.should();

describe("getArgumentName", () => {
	describe("Normal functions", () => {
		it("should return empty array if function has no arguments", () => {
			const subj = function () {};
			getArgumentName(subj).should.deep.equal([]);
		});

		it("should extract parameter names as array", () => {
			const subj1 = function (a, b, c) {};
			const subj2 = function (a) {};
			const subj3 = function (a1, verylong_name) {};
			const subj4 = function ($apple) {};
			getArgumentName(subj1).should.deep.equal(["a", "b", "c"]);
			getArgumentName(subj2).should.deep.equal(["a"]);
			getArgumentName(subj3).should.deep.equal(["a1", "verylong_name"]);
			getArgumentName(subj4).should.deep.equal(["$apple"]);
		});

		it("should work with spaces and tabs", () => {
			const subj1 = function  (a,  b,   c) {};
			const subj2 = function(a,b,c){         };
			const subj3 = function		(a	,	b,			c)		{		};
			getArgumentName(subj1).should.deep.equal(["a", "b", "c"]);
			getArgumentName(subj2).should.deep.equal(["a", "b", "c"]);
			getArgumentName(subj3).should.deep.equal(["a", "b", "c"]);
		});

		it("should work with inline comments", () => {
			const subj1 = function (/*a,b,c*/) {};
			const subj2 = function (/*a,b,c*/d, e) {};
			const subj3 = function (a, b /*,c,d,e*/) {};
			const subj4 = function (a, /*b,c,*/ d) {};
			getArgumentName(subj1).should.deep.equal([]);
			getArgumentName(subj2).should.deep.equal(["d", "e"]);
			getArgumentName(subj3).should.deep.equal(["a", "b"]);
			getArgumentName(subj4).should.deep.equal(["a", "d"]);
		});

	});

	describe("Arrow functions", () => {
		it("should return empty array if function has no arguments", () => {
			const subj = () => {};
			getArgumentName(subj).should.deep.equal([]);
		});

		it("should extract parameter names as array", () => {
			const subj1 = (a, b, c) => {};
			const subj2 = (a) => {};
			const subj3 = (a1, verylong_name) => {};
			const subj4 = ($apple) => {};
			getArgumentName(subj1).should.deep.equal(["a", "b", "c"]);
			getArgumentName(subj2).should.deep.equal(["a"]);
			getArgumentName(subj3).should.deep.equal(["a1", "verylong_name"]);
			getArgumentName(subj4).should.deep.equal(["$apple"]);
		});

		it("should work with spaces and tabs", () => {
			const subj1 = (a,  b,   c)    => {};
			const subj2 = (a,b,c)=>{};
			const subj3 = (a	,	b,			c)		=>		{		};
			getArgumentName(subj1).should.deep.equal(["a", "b", "c"]);
			getArgumentName(subj2).should.deep.equal(["a", "b", "c"]);
			getArgumentName(subj3).should.deep.equal(["a", "b", "c"]);
		});

		it("should work with inline comments", () => {
			const subj1 = (/*a,b,c*/) => {};
			const subj2 = (/*a,b,c*/d, e) => {};
			const subj3 = (a, b /*,c,d,e*/) => {};
			const subj4 = (a, /*b,c,*/ d) => {};
			getArgumentName(subj1).should.deep.equal([]);
			getArgumentName(subj2).should.deep.equal(["d", "e"]);
			getArgumentName(subj3).should.deep.equal(["a", "b"]);
			getArgumentName(subj4).should.deep.equal(["a", "d"]);
		});

	});

});
