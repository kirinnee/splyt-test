const {should} = require("chai"),
	defaultArguments = require("..");

should();

describe('Default Argument', function () {

	function add(a, b) {
		return a + b;
	}

	describe("no default given", () => {
		const add2 = defaultArguments(add);
		it("should add 2 numbers together, given 2 numbers", () => {
			add2(1, 2).should.deep.equal(3);
		});

		it('should be NaN if any of the number is not provided', () => {
			Number.isNaN(add2()).should.be.true;
			Number.isNaN(add2(1)).should.be.true;
			Number.isNaN(add2(undefined, 1)).should.be.true;
		});
	});

	describe("one relevant default given", () => {
		const add2 = defaultArguments(add, {b: 9});
		it('should use the default when not given', () => {
			add2(10).should.deep.equal(19);
		});

		it('should not use the default when value is given', () => {
			add2(10, 7).should.deep.equal(17);
		});

		it('should return NaN when a value is not given, but no default is found', () => {
			Number.isNaN(add2(null, 10)).should.be.true
		});
	});

	describe("both relevant default given", () => {
		const add2 = defaultArguments(add, {b: 3, a: 2});
		it('should use the default when not given', () => {
			add2(10).should.deep.equal(13);
			add2().should.deep.equal(5);
			add2(null, 50).should.deep.equal(52);
		});

		it('should not use the default when value is given', () => {
			add2(7,20).should.deep.equal(27);
		});
	});

	describe("non-relevant default given",()=>{
		const add2 = defaultArguments(add, {c: 12});
		it("should add 2 numbers together, given 2 numbers", () => {
			add2(1, 2).should.deep.equal(3);
		});

		it('should be NaN if any of the number is not provided', () => {
			Number.isNaN(add2()).should.be.true;
			Number.isNaN(add2(1)).should.be.true;
			Number.isNaN(add2(undefined, 1)).should.be.true;
		});
	});


});
