const {getArgumentName} = require("./src/argumentName");

function defaultArguments(func, def) {
	if (def == null) def = {};
	const argumentNames = getArgumentName(func);
	return function (...a) {
		for (let i = 0; i < argumentNames.length; i++) {
			if (a[i] == null) {
				a[i] = def[argumentNames[i]];
			}
		}
		return func(...a);
	}
}

module.exports = defaultArguments;
