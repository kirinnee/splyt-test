export enum CollisionType {
	IntersectFront = "ifront",
	IntersectBack = "iback",
	SuperSet = "superset",
	Subset = "subset",
	Front = "front",
	Back = "back",
	Match = "match",
}
