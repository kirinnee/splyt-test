import {CollisionType} from "./CollisionType";
import {tickToTime} from "./time";

class Slot {
    start: number;
    end: number;
    next: Slot | null = null;
    prev: Slot | null = null;

    constructor(start: number, end: number) {
        this.start = start;
        this.end = end;
    }

    get Duration(): number {
        return this.end - this.start;
    }

    get Enough(): boolean {
        return this.Duration >= 60;
    }

    Delete(): void {
        if (this.prev != null) {
            if (this.next != null) {
                this.prev.next = this.next;
                this.next.prev = this.prev;
            } else {
                this.prev.next = null;
            }
        } else {
            if (this.next != null) {
                this.next.prev = null;
            }
        }
        this.next = null;
        this.prev = null;
    }

    Insert(slot: Slot): void {
        if (this.next == null) {
            this.next = slot;
            slot.prev = this;
        } else {
            this.next.prev = slot;
            slot.next = this.next;
            slot.prev = this;
            this.next = slot;
        }
    }

    /*
     Returns if you should continue on the same person
     */
    Inject(slot: Slot): void {
        const col = slot.Intersect(this);
        let end: boolean = false;
        switch (col) {
            case CollisionType.IntersectFront:
                this.start = slot.end;
                end = true;
                break;
            case CollisionType.IntersectBack:
                this.end = slot.start;
                break;
            case CollisionType.Front:
                return;
            case CollisionType.Back:
                break;
            case CollisionType.SuperSet:
                this.end = this.start + 1;
                break;
            case CollisionType.Match:
                this.end = this.start + 1;
                end = true;
                break;
            case CollisionType.Subset:
                const x = new Slot(slot.end, this.end);
                this.end = slot.start;

                if (x.Enough) {
                    this.Insert(x);
                }
                break;
        }
        if (!this.Enough)
            this.Delete();
        if (end) return;
        if (this.next != null) {
            this.next.Inject(slot);
        }
    }


    Intersect(slot: Slot): CollisionType {
        if (this.end < slot.start) return CollisionType.Front;
        if (this.start > slot.end) return CollisionType.Back;
        if (this.start == slot.start && this.end == slot.end) return CollisionType.Match;
        let front = this.start < slot.start;
        let back = this.end > slot.end;
        if (front) {
            return back ? CollisionType.SuperSet : CollisionType.IntersectFront;
        } else {
            return back ? CollisionType.IntersectBack : CollisionType.Subset;
        }
    }

    Traverse(): string {
        if (this.next != null) {
            return this.next.Traverse();
        } else {
            return this.Start;
        }
    }

    get Start(): string {
        return tickToTime(this.start);
    }


    get End(): string {
        return tickToTime(this.end);
    }


}

export {Slot}
