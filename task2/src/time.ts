export function timeToTicks(time: string): number {
	const hm: string[] = time.split(":");
	const [h, m] = hm.map(e => parseInt(e, 10));
	return (h - 9) * 60 + m;
}

export function tickToTime(ticks: number) : string {
	const hour = Math.floor(ticks/ 60) + 9;
	const min = ticks % 60;
	let out = "";
	out += hour < 10 ? "0" + hour : hour;
	out += ":";
	out += min < 10 ? "0" + min : min;
	return out;
}
