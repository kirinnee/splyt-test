import {should} from "chai";
import {Slot} from "../src/Slots";
import {timeToTicks} from "../src/time";
import {CollisionType} from "../src/CollisionType";

should();

function NewSlot(s: string, e: string): Slot {
	return new Slot(timeToTicks(s), timeToTicks(e));
}

describe('Slot', () => {
	describe('duration', () => {
		it('should return the duration of the slot', () => {
			const s1 = new Slot(0, 1);
			const s2 = NewSlot("11:11", "12:11");
			s1.Duration.should.equal(1);
			s2.Duration.should.equal(60);
		});
	});
	
	describe('enough', () => {
		it('should return true if there is at least 60 min', () => {
			const s1 = NewSlot("11:11", "17:20");
			const s2 = NewSlot("11:11", "12:11");
			s1.Enough.should.be.true;
			s2.Enough.should.be.true;
		});
		it('should return false if there is less than 60 min', () => {
			const s1 = NewSlot("11:11", "12:10");
			const s2 = NewSlot("11:11", "11:20");
			s1.Enough.should.be.false;
			s2.Enough.should.be.false;
		});
	});
	
	describe("Intersect", () => {
		it('should return Front if its in-front', () => {
			const actual = NewSlot("09:00", "10:00")
				.Intersect(NewSlot("11:00", "12:00"));
			actual.should.deep.equal(CollisionType.Front);
		});
		
		it('should return Back if it is behind', () => {
			const actual = NewSlot("13:30", "15:50")
				.Intersect(NewSlot("11:00", "12:00"));
			actual.should.deep.equal(CollisionType.Back);
		});
	});
	it('should return Match if they are the same', () => {
		const actual = NewSlot("13:30", "15:50")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.Match);
	});
	it('should return SuperSet if its start is earlier and end is later', () => {
		const actual = NewSlot("13:29", "15:52")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.SuperSet);
	});
	
	it('should return SubSet if its start is later and end is earlier', () => {
		const actual = NewSlot("13:32", "15:30")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.Subset);
	});
	
	it('should return SubSet if its start same and end is earlier', () => {
		const actual = NewSlot("13:30", "15:30")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.Subset);
	});
	
	it('should return SubSet if its start later and end is same', () => {
		const actual = NewSlot("13:35", "15:50")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.Subset);
	});
	
	it('should return Intersectfront if it starts earlier and end same', () => {
		const actual = NewSlot("13:00", "15:50")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.IntersectFront);
	});
	
	
	it('should return IntersectFront if it starts earlier and end earlier', () => {
		const actual = NewSlot("13:00", "15:40")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.IntersectFront);
	});
	
	
	it('should return IntersectBack if it starts same and end later', () => {
		const actual = NewSlot("13:30", "18:50")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.IntersectBack);
	});
	
	
	it('should return IntersectFront if it starts later and end later', () => {
		const actual = NewSlot("14:20", "18:40")
			.Intersect(NewSlot("13:30", "15:50"));
		actual.should.deep.equal(CollisionType.IntersectBack);
	});
	
});

describe("Insert", () => {
	it('should add the to the end, if the node is the last node', () => {
		const n1 = NewSlot("09:00", "10:00");
		const n2 = NewSlot("11:00", "12:00");
		n1.Insert(n2);
		n1.next!.should.equal(n2);
		(n1.prev == null).should.be.true;
		n2.prev!.should.equal(n1);
		(n2.next == null).should.be.true;
	});
	
	it("should inject in the middle if the node is not the last node", () => {
		const n1 = NewSlot("09:00", "10:00");
		const n2 = NewSlot("11:00", "12:00");
		const n3 = NewSlot("14:00", "15:00");
		n1.Insert(n3);
		n1.Insert(n2);
		n1.next!.should.equal(n2);
		n2.prev!.should.equal(n1);
		n2.next!.should.equal(n3);
		n3.prev!.should.equal(n2);
		(n1.prev == null).should.be.true;
		(n3.next == null).should.be.true;
	});
});

describe("Delete", () => {
	it('should remove the current node, pointer the prev to next node and vise versa', () => {
		const n1 = NewSlot("09:00", "10:00");
		const n2 = NewSlot("11:00", "12:00");
		const n3 = NewSlot("14:00", "15:00");
		n1.Insert(n3);
		n1.Insert(n2);
		n2.Delete();
		n1.next!.should.equal(n3);
		n3.prev!.should.equal(n1);
		(n1.prev == null).should.be.true;
		(n3.next == null).should.be.true;
	});
	it('should remove the current node, pointer the next nodes prev node to null', () => {
		const n1 = NewSlot("09:00", "10:00");
		const n2 = NewSlot("11:00", "12:00");
		const n3 = NewSlot("14:00", "15:00");
		n1.Insert(n3);
		n1.Insert(n2);
		n1.Delete();
		n2.next!.should.equal(n3);
		n3.prev!.should.equal(n2);
		(n2.prev == null).should.be.true;
		(n3.next == null).should.be.true;
	});
	
	it('should remove the current node, prev node pointer to null',()=>{
		const n1 = NewSlot("09:00", "10:00");
		const n2 = NewSlot("11:00", "12:00");
		const n3 = NewSlot("14:00", "15:00");
		n1.Insert(n3);
		n1.Insert(n2);
		n3.Delete();
		n1.next!.should.equal(n2);
		n2.prev!.should.equal(n1);
		(n1.prev == null).should.be.true;
		(n2.next == null).should.be.true;
		
	});
	
});
