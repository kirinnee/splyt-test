import {tickToTime, timeToTicks} from "../src/time";
import {should} from "chai";

should();

describe('timeToTicks', () => {
	it('should convert time to 0900 normalized minutes', () => {
		const s1 = '09:00', s2 = '11:23', s3 = '19:00', s4 = '14:47';
		const e1 = 0, e2 = 143, e3 = 600, e4 = 347;
		timeToTicks(s1).should.equal(e1);
		timeToTicks(s2).should.equal(e2);
		timeToTicks(s3).should.equal(e3);
		timeToTicks(s4).should.equal(e4);
	});
});


describe('ticksToTime', () => {
	it('should convert tick to 0900 normalized time', () => {
		const e1 = '09:00', e2 = '11:23', e3 = '19:00', e4 = '14:47';
		const s1 = 0, s2 = 143, s3 = 600, s4 = 347;
		tickToTime(s1).should.equal(e1);
		tickToTime(s2).should.equal(e2);
		tickToTime(s3).should.equal(e3);
		tickToTime(s4).should.equal(e4);
	});
});
