import {Slot} from "./src/Slots";
import {timeToTicks} from "./src/time";

function stringToSlot([start, end]: [string, string]): Slot {
    return new Slot(timeToTicks(start), timeToTicks(end));
}

export function getFreeTime(schedule: [string, string][][]) {
    const root = new Slot(0, 0);
    const main = new Slot(0, 600);
    root.Insert(main);

    for (let i = 0; i < schedule.length; i++) {

        const person: Slot[] = schedule[i].map(stringToSlot);

        for (const d of person) {
            if (root.next != null) {
                root.next!.Inject(d);
            } else {
                return null;
            }
        }
    }
    if (root.next == null) return null;
    return root.next!.Traverse();
}

let schedules: [string, string][][] = [
    [['09:00', '12:35'], ['13:30', '16:00'], ['16:00', '17:30'], ['17:30', '17:45']],
    [['09:15', '12:00'], ['14:00', '16:30'], ['17:00', '17:30']],
    [['11:30', '12:15'], ['15:00', '16:30'], ['17:45', '18:00']]
];

console.log(getFreeTime(schedules));

